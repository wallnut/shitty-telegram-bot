﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TeamTUSKFeederBot
{
    class ChatLog
    {
        private string file = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/ChatLog.txt";


        public void AddToLog(string line)
        {
            StreamWriter writer = new StreamWriter(file, true);
            writer.WriteLine(line);
            writer.Close();
        }
        public void AddToLog(List<string> lines)
        {
            StreamWriter writer = new StreamWriter(file, true);
            for (int index = 0; index < lines.Count; index++)
            {
                writer.WriteLine(lines[index]);
            }
            writer.Close();
        }
    }
}
