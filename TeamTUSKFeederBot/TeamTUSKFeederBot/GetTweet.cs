﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TweetSharp;
using System.Diagnostics;

namespace TeamTUSKFeederBot
{
    class GetTweet
    {
        /// <summary>
        /// Reads the twitter app API tokens from TwitterAPI.conf
        /// </summary>
        /// <returns>The tokens in an OAuthAccessToken object</returns>
        private OAuthAccessToken GetAPITokens()
        {
            string file = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/TwitterAPI.conf";
            List<string> twitter = File.ReadAllLines(file).ToList();
            OAuthAccessToken get = new OAuthAccessToken();
            bool accessTokenRead = false;
            bool accessSecretRead = false;

            for (int index = 0; index < 2; index++)
            {
                string[] temp = new string[1];
                temp = twitter[index].Split('=');
                if (temp[0].ToLower().CompareTo("accesstoken") == 0)
                {
                    get.Token = temp[1];
                    accessTokenRead = true;
                }
                if (temp[0].ToLower().CompareTo("accesstokensecret") == 0)
                {
                    get.TokenSecret = temp[1];
                    accessSecretRead = true;
                }
            }
            if ((accessSecretRead == false) || (accessTokenRead == false))
            {
                Console.WriteLine("Error: Failed reading twitter app API keys from file.");
                //Console.WriteLine(get.Token);
                //Console.WriteLine(get.TokenSecret);
                Console.ReadKey(true);
                Environment.Exit(0);
            }

            return get;
        }

        /// <summary>
        /// Creates a user, writes his info to file, then returns his info to the program as well.
        /// </summary>
        /// <param name="service">The twitter API</param>
        /// <param name="accessToken"></param>
        /// <param name="accessTokenSecret"></param>
        /// <returns></returns>
        private OAuthAccessToken CreateUser(TwitterService service, string accessToken, string accessTokenSecret)
        {
            OAuthRequestToken requestToken = service.GetRequestToken();
            Uri uri = service.GetAuthorizationUri(requestToken);
            Process.Start(uri.ToString());
            service.AuthenticateWith(accessToken, accessTokenSecret);

            Console.Write("Please input the PIN: ");
            string verifier = Console.ReadLine();
            OAuthAccessToken access = service.GetAccessToken(requestToken, verifier);

            List<string> userFile = new List<string>();
            userFile.Add("userToken=" + access.Token);
            userFile.Add("userTokenSecret=" + access.TokenSecret);
            userFile.Add("screenName=" + access.ScreenName);
            userFile.Add("userID=" + access.UserId);
            File.WriteAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/TwitterUser.conf", userFile);

            return access;
        }

        private OAuthAccessToken GetUser()
        {
            List<string> twitterUserFile = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/TwitterUser.conf").ToList();
            bool accessTokenRead = false;
            bool accessSecretRead = false;
            bool screenNameRead = false;
            bool userIDRead = false;

            OAuthAccessToken returnToken = new OAuthAccessToken();
            for (int index = 0; index < 4; index++)
            {
                string[] temp = new string[1];
                temp = twitterUserFile[index].Split('=');
                if (temp[0].ToLower().CompareTo("usertoken") == 0)
                {
                    accessTokenRead = true;
                    returnToken.Token = temp[1];
                }
                if (temp[0].ToLower().CompareTo("usertokensecret") == 0)
                {
                    accessSecretRead = true;
                    returnToken.TokenSecret = temp[1];
                }
                if (temp[0].ToLower().CompareTo("screenname") == 0)
                {
                    screenNameRead = true;
                    returnToken.ScreenName = temp[1];
                }
                if (temp[0].ToLower().CompareTo("userid") == 0)
                {
                    userIDRead = true;
                    returnToken.UserId = int.Parse(temp[1]);
                }
            }
            if ((accessSecretRead == false) || (accessTokenRead == false) || (screenNameRead == false) || (userIDRead == false))
            {
                Console.WriteLine("Error: Failed reading twitter user API keys from file.");
                Console.ReadKey(true);
                Environment.Exit(0);
            }

            return returnToken;

        }

        public void InitializeTwitter()
        {
            OAuthAccessToken keys = GetAPITokens();
            string accessToken = keys.Token;
            string accessTokenSecret = keys.TokenSecret;
            TwitterService service = new TwitterService(accessToken, accessTokenSecret);
            CreateUser(service, accessToken, accessTokenSecret);
        }

        public string GetRandomTweet(bool verbose)
        {
            OAuthAccessToken keys = GetAPITokens();
            string accessToken = keys.Token;
            string accessTokenSecret = keys.TokenSecret;
            TwitterService service = new TwitterService(accessToken, accessTokenSecret);

            OAuthAccessToken access = GetUser();
            service.AuthenticateWith(access.Token, access.TokenSecret);
            if(verbose)
            {
                Console.WriteLine("@" + access.ScreenName + " signed into twitter.");
            }

            Random randomSelection = new Random();

            var tweets = service.ListTweetsOnHomeTimeline(new ListTweetsOnHomeTimelineOptions());
            var tweetList = tweets.ToList();
            string randomTweeter = "";
            do
            {
                int randomIndex = randomSelection.Next((tweetList.Count - 1));
                randomTweeter = tweetList[randomIndex].Text;
            } while (randomTweeter.Contains("@"));

            return randomTweeter;
            
        }
    }
}
