﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace TeamTUSKFeederBot
{
    class TUSKBot
    {
        public bool QuietMode { get; set; } = false;
        private int randomCooldown = 0;
        /// <summary>
        /// Posts the image from the TUSK/Images directory
        /// </summary>
        /// <param name="usingBot">The bot object which has the API key instanciated</param>
        /// <param name="groupChatId">the ID of the chat to post the image in</param>
        /// <param name="imageName">filename of the image in TUSK/Images</param>
        /// <param name="verbose">Just pass the parents' verbose variable</param>
        private async void PostImage(Telegram.Bot.Api usingBot, int groupChatId, string imageName, bool verbose)
        {
            string ImagePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Images/" + imageName;

            Telegram.Bot.Types.FileToSend photoMaybe = new Telegram.Bot.Types.FileToSend();
            photoMaybe.Filename = ImagePath;
            Stream imageStream = File.OpenRead(ImagePath);
            photoMaybe.Content = imageStream;
            photoMaybe.Filename = imageName;


            try
            {
                await usingBot.SendPhoto(groupChatId, photoMaybe);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey(true);
            }
            if (verbose)
            {
                Console.WriteLine("Finished");
            }
            System.Threading.Thread.Sleep(1000);
        }
        /// <summary>
        /// Checks if two strings are the same, the difference between this and compare is that this is not case sensitive
        /// </summary>
        /// <returns></returns>
        private bool CheckForWord(string firstString, string secongstring)
        {
            firstString = firstString.ToLower();
            secongstring = secongstring.ToLower();
            if (firstString.Contains(secongstring))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets updates via Telegram API
        /// </summary>
        /// <param name="usingBot">The bot object which has the API key instanciated</param>
        /// <param name="verbose">Just pass the parents' verbose variable</param>
        private async void ShowUpdates(Telegram.Bot.Api usingBot, bool verbose)
        {
            ChatLog chat = new ChatLog();
            List<string> temp = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt").ToList();

            int lastReadMessage;
            try
            {
                lastReadMessage = Convert.ToInt32(temp[0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Data);
                lastReadMessage = 0;
            }

            int checkLastReadMessage = lastReadMessage;

            var newUpdates = await usingBot.GetUpdates(lastReadMessage + 1);
            List<Telegram.Bot.Types.Update> tuskList = newUpdates.ToList();

            try
            {
                bool catcher = tuskList[0] == null;
            }
            catch (Exception) /* No new messages got from Telegram */
            { }

            string chatString = "";
            int index = 0;
            List<string> chatLog = new List<string>();

            for (index = 0; index < newUpdates.Length; index++)
            {
                chatString = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " Message ID: " + tuskList[index].Id + ". " + tuskList[index].Message.From.FirstName + ": " + tuskList[index].Message.Text;

                try
                {
                    chatLog.Add(chatString);
                    if (verbose)
                    {
                        Console.WriteLine(chatString);
                    }
                }
                catch (Exception ex)
                {
                    if (true)
                    {
                        Console.WriteLine(ex.Data);
                    }
                }

                Random rngBased = new Random();
                #region
                if (QuietMode == false)
                {
                    if (rngBased.Next(99) == 7)
                    {
                        GetTweet twotter = new GetTweet();
                        string tweet = twotter.GetRandomTweet(verbose);
                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, tweet);
                    }
                    if (CheckForWord(chatString, "gold"))
                    {
                        if (rngBased.Next(3) == 0)
                        {
                            await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "BUY GOLD!!!");
                        }
                    }
                    if (rngBased.Next(600) == 64)
                    {
                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "Secret will never lose");
                    }
                    if (rngBased.Next(1000) == 322)
                    {
                        string naga = "Learn to play , and never play naga again u noob . "
                            + "dota is a game about 5 player not 1 farming and farming , "
                            + "team need all 5 players fight , thx for trow the game noob useless";
                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, naga);
                    }
                    if (rngBased.Next(50) == 5)
                    {
                        if (randomCooldown == 0)
                        {
                            await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "I love ice cream!");
                            randomCooldown = 2;
                        }
                        else
                        {
                            randomCooldown--;
                        }
                    }
                    if (CheckForWord(chatString, "fuck") && CheckForWord(chatString, "you"))
                    {
                        if (rngBased.Next(3) == 2)
                        {
                            await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "Wasn't even camping!");
                        }
                    }
                    if (CheckForWord(chatString, "pizza"))
                    {
                        if (rngBased.Next(3) == 2)
                            PostImage(usingBot, tuskList[index].Message.Chat.Id, "Pizza.png", verbose);
                    }
                    if (CheckForWord(chatString, "skrillex") || CheckForWord(chatString, "pineapple"))
                    {
                        if (rngBased.Next(3) == 2)
                            PostImage(usingBot, tuskList[index].Message.Chat.Id, "Skrillex.jpg", verbose);
                    }
                    if (CheckForWord(chatString, "haha"))
                    {
                        if (rngBased.Next(3) == 2)
                            await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "What's so funny, punk?");
                    }
                    if (CheckForWord(chatString, "/din5"))
                    {
                        PostImage(usingBot, tuskList[index].Message.Chat.Id, "Din5.jpg", verbose);
                    }
                    if (CheckForWord(chatString, "salt"))
                    {
                        if (rngBased.Next(3) == 2)
                            PostImage(usingBot, tuskList[index].Message.Chat.Id, "Salt.png", verbose);
                    }
                    if (CheckForWord(chatString, "/sluttyanimewaifuinathong"))
                    {
                        PostImage(usingBot, tuskList[index].Message.Chat.Id, "Slut.jpg", verbose);
                    }
                    if (CheckForWord(chatString, "bow"))
                    {
                        if (rngBased.Next(3) == 2)
                            PostImage(usingBot, tuskList[index].Message.Chat.Id, "Bow.jpg", verbose);
                    }
                    if (CheckForWord(chatString, "adam"))
                    {
                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, "What a cuck, right?");
                    }
                    if (CheckForWord(chatString, "/mycucks"))
                    {
                        CuckWork tuskWorker = new CuckWork();
                        int numberOfCucks = tuskWorker.FindCuckPoints(tuskList[index].Message.From.Id);
                        string cuckMessage = tuskList[index].Message.From.FirstName + " has " + numberOfCucks + " cuck points!";
                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, cuckMessage);
                    }
                    else if (CheckForWord(chatString, "/cuckmaster"))
                    {
                        CuckWork cuckWorker = new CuckWork();
                        MasterCuck masterCuck = cuckWorker.MasterCuck();

                        await usingBot.SendTextMessage(tuskList[index].Message.Chat.Id, masterCuck.Name + " is the cuck master!\nWith " + masterCuck.Points + " cucks!");
                    }
                    else if (CheckForWord(chatString, "cuck"))
                    {
                        CuckWork tuskWorker = new CuckWork();
                        tuskWorker.Cucker(tuskList[index].Message.From.Id, tuskList[index].Message.From.FirstName);
                    }
                }
                #endregion
            }

            chat.AddToLog(chatLog);


            try
            {
                lastReadMessage = tuskList[(tuskList.Count - 1)].Id;
            }
            catch (ArgumentOutOfRangeException) /* No new messages to write to file */
            {
            }


            

            try
            {
                StreamWriter lastMessage = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                + @"/TUSK/LastReadMessage.txt");
                lastMessage.WriteLine(lastReadMessage);
                lastMessage.Close();
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Error: Failed writing last read message to file. Weird stuff might happen from here on in.");
            }

        }

        /// <summary>
        /// Starts the bot
        /// </summary>
        /// <param name="usingBot">The bot object which has the API key instanciated</param>
        /// <param name="verbose">Just pass the parents' verbose variable</param>
        public void RunBot(Telegram.Bot.Api usingBot, bool verbose)
        {
            while (true)
            {
                ShowUpdates(usingBot, verbose);
                System.Threading.Thread.Sleep(1500);
            }
        }
    }
}
