﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TeamTUSKFeederBot
{
    class Launch
    {
        /// <summary>
        /// Checks if all the files/directories required for the bot to operate exist, and if they don't, it creates them
        /// </summary>
        public static void Check(bool verbose)
        {
            #region
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK");
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Images");
                if (verbose)
                {
                    Console.WriteLine("TUSK directory does not exist. Created.");
                }
            }
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Images"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Images");
                if (verbose)
                {
                    Console.WriteLine("Images directory does not exist. Created.");
                }
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt"))
            {
                //File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt");
                List<string> initializeImportantFile = new List<string>();
                initializeImportantFile.Add("0");
                File.WriteAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt", initializeImportantFile);
                if (verbose)
                {
                    Console.WriteLine("Last read message file does not exist. Created and initialized.");
                }
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/ChatLog.txt"))
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/ChatLog.txt");
                if (verbose)
                {
                    Console.WriteLine("Chat log does not exist. Created.");
                }
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Cuckpoints.txt"))
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Cuckpoints.txt");
                if (verbose)
                {
                    Console.WriteLine("Cuck point list does not exist. Created.");
                }
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/API.txt"))
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/API.txt");
                if (verbose)
                {
                    Console.WriteLine("API key file does not exist. File created.");
                    Console.WriteLine("\tHOWEVER: The bot will not function until the file contains a valid API key and only that");
                }
            }
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/TwitterAPI.conf"))
            {
                GetTweet initialize = new GetTweet();
                if (verbose)
                {
                    Console.WriteLine("Associating a twitter user.");
                }
                initialize.InitializeTwitter();
            }
            #endregion

            List<string> importantFile = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt").ToList();
            try /* The weirdest try catch I've written in my life */
            {
                if (importantFile[0] == "") ;
            }
            catch (ArgumentOutOfRangeException)
            {
                List<string> initializeImportantFile = new List<string>();
                initializeImportantFile.Add("0");
                File.WriteAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt", initializeImportantFile);

            }
            try
            {
                int legitimacyCheck = int.Parse(importantFile[0]);
            }
            catch (Exception ex)
            {
                if (verbose)
                {
                    Console.WriteLine(ex.Data);
                }
                List<string> initializeImportantFile = new List<string>();
                initializeImportantFile.Add("0");
                File.WriteAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/LastReadMessage.txt", initializeImportantFile);

            }
        }
    }
}
