﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TeamTUSKFeederBot
{
    class MasterCuck
    {
        public string Name { get; set; }
        public int Points { get; set;}
    }
    class CuckWork
    {
        private List<string> cuckFile;

        /// <summary>
        /// Reads a user's cucks from THE file
        /// </summary>
        /// <returns></returns>
        private void ReadCucks()
        {
            cuckFile = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Cuckpoints.txt").ToList();
        }

        /// <summary>
        /// Increases a users cuck points
        /// </summary>
        /// <param name="userID">the ID of the user to change the points of</param>
        /// <param name="userName">user's name</param>
        /// <param name="pointsToAdd">how many points to add to said user</param>
        /// <returns></returns>
        private void AddToCuck(int userID, string userName, int pointsToAdd)
        {
            bool userExistsInCucks = false;
            for(int index = 0; index < cuckFile.Count; index++)
            {
                string[] splitter = new string[2];
                splitter = cuckFile[index].Split(':');
                if((int.Parse(splitter[0]) == userID) && userExistsInCucks == false)
                {
                    userExistsInCucks = true;
                    cuckFile.RemoveAt(index);
                    splitter[1] = Convert.ToString(int.Parse(splitter[1]) + 1);
                    string compiledSplitter = splitter[0] + ":" + splitter[1] + ":" + splitter[2];
                    cuckFile.Add(compiledSplitter);
                }
            }

            if(userExistsInCucks == false)
            {
                string newUser = userID + ":1:" + userName;
                cuckFile.Add(newUser);
                userExistsInCucks = true;
            }
            
        }

        /// <summary>
        /// Writes the cuck list to file
        /// </summary>
        private void WriteCucks()
        {
            string file = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/Cuckpoints.txt";
            File.WriteAllLines(file, cuckFile);
        }

        /// <summary>
        /// Finds how many cuck points a user has
        /// </summary>
        /// <param name="userID">The user's id for checking</param>
        /// <returns></returns>
        public int FindCuckPoints(int userID)
        {
            ReadCucks();
            int returnNum = 0;
            for(int index = 0; index < cuckFile.Count; index++)
            {
                string[] splitter = new string[2];
                splitter = cuckFile[index].Split(':');
                if(int.Parse(splitter[0]) == userID)
                {
                    returnNum = int.Parse(splitter[1]);
                }
            }
            return returnNum;
        }

        public MasterCuck MasterCuck()
        {
            MasterCuck cuckMaestro = new MasterCuck();
            int maxCucks = 0;
            ReadCucks();
            for(int index = 0; index < cuckFile.Count; index++)
            {
                string[] temp = new string[2];
                temp = cuckFile[index].Split(':');
                if(int.Parse(temp[1]) > maxCucks)
                {
                    maxCucks = int.Parse(temp[1]);
                    cuckMaestro.Name = temp[2];
                    cuckMaestro.Points = maxCucks;
                }
            }
            return cuckMaestro;
        }

        public void Cucker(int userID, string name)
        {
            ReadCucks();
            AddToCuck(userID, name, 1);
            WriteCucks();
        }
    }
}
