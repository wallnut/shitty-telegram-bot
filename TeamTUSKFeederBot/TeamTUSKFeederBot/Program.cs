﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram;
using System.IO;

namespace TeamTUSKFeederBot
{
    class Program
    {
        static void Main(string[] args)
        {
            string copyLeft = "Copyright(C) 2015  Emile <emilisbliudzius@gmail.com>\nThis program comes with ABSOLUTELY NO WARRANTY."
                + "\nThis is free software, and you are welcome to redistribute it" +
                " under certain conditions;\nRead the LICENSE.md file for details.\n\n";

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(copyLeft);
            Console.ForegroundColor = ConsoleColor.White;
            TUSKBot tuskChatter = new TUSKBot();
            string accessToken = "";
            bool verbose = args.Contains("-v");

            if (args.Contains("-q") || args.Contains("--quiet"))
            {
                tuskChatter.QuietMode = true;
                if (verbose)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("TUSKBot running in spy mode.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }

            try
            {
                string[] API_KEY = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"/TUSK/API.txt");
                accessToken = API_KEY[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Data + " During bot initialization. Probably a missing API key.");
                Console.WriteLine(@"Hint: %APPDATA%/TUSK/API.txt");
                if (!args.Contains("--skip-launch-check"))
                {
                    Launch.Check(verbose);
                }
                Console.ReadKey(true);
                Environment.Exit(0);
            }
            Telegram.Bot.Api tuskerinoBotterino = new Telegram.Bot.Api(accessToken);


            if (!args.Contains("--skip-launch-check") && !args.Contains("-sl"))
            {
                Launch.Check(verbose);
            }
            if (args.Contains("-h") || args.Contains("--help"))
            {
                Console.WriteLine("-v\tVerbose mode.");
                Console.WriteLine("-h | --help\tShow this menu");
                Console.WriteLine("-sl | --skip-launch-check\tSkips the file check at launch");
                Environment.Exit(0);
            }
            tuskChatter.RunBot(tuskerinoBotterino, verbose);
            Console.ReadKey();

        }
    }
}
